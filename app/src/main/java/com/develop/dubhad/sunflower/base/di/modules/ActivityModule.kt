package com.develop.dubhad.sunflower.base.di.modules

import com.develop.dubhad.sunflower.authentication.di.AuthFragmentBuildersModule
import com.develop.dubhad.sunflower.authentication.ui.AuthenticationActivity
import com.develop.dubhad.sunflower.base.ui.StartActivity
import com.develop.dubhad.sunflower.main.base.di.MainFragmentBuildersModule
import com.develop.dubhad.sunflower.main.base.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [AuthFragmentBuildersModule::class])
    abstract fun contributeAuthenticationActivity(): AuthenticationActivity

    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeStartActivity(): StartActivity
}
