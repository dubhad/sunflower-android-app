package com.develop.dubhad.sunflower.authentication.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TokenInfoResponse(@Json(name = "key") val key: String?)
