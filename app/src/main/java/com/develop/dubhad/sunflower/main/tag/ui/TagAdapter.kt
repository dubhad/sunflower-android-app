package com.develop.dubhad.sunflower.main.tag.ui

import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.main.tag.models.domain.Tag
import com.develop.dubhad.sunflower.utils.extensions.inflate
import kotlinx.android.synthetic.main.item_tag.view.*

class TagAdapter : ListAdapter<Tag, TagAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_tag))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tag = getItem(position)
        holder.bind(createOnClickListener(tag.id, tag.title), tag)
    }

    private fun createOnClickListener(tagId: Int, tagTitle: String): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                TagListFragmentDirections.actionTagListFragmentToRecipeListFragment(
                    tagId = tagId,
                    title = tagTitle
                )
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listener: View.OnClickListener, item: Tag) {
            with(itemView) {
                setOnClickListener(listener)
                tag_item_title.text = item.title
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Tag>() {

            override fun areItemsTheSame(oldItem: Tag, newItem: Tag): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Tag, newItem: Tag): Boolean {
                return oldItem == newItem
            }
        }
    }
}
