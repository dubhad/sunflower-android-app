package com.develop.dubhad.sunflower.main.recipe.base.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.local.models.RecipeStepEntity
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.RecipeStepResponse
import com.develop.dubhad.sunflower.utils.ParsingUtil

data class RecipeStep(
    val id: Int,
    val description: String,
    val photo: String,
    val number: Int
) {
    constructor(response: RecipeStepResponse) : this(
        id = response.recipeStepId ?: throw ParsingServerException(),
        description = response.description ?: throw ParsingServerException(),
        photo = response.photo ?: throw ParsingServerException(),
        number = response.stepNumber ?: throw ParsingServerException()
    ) {
        if (ParsingUtil.isAnyNumberZero(id, number) ||
            ParsingUtil.isAnyStringBlank(description)
        ) {
            throw ParsingServerException()
        }
    }
    constructor(entity: RecipeStepEntity) : this(
        id = entity.id,
        description = entity.description,
        photo = entity.photo,
        number = entity.number
    )
}
