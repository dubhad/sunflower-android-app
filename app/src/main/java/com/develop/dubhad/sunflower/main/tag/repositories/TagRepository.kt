package com.develop.dubhad.sunflower.main.tag.repositories

import com.develop.dubhad.sunflower.base.services.SunflowerApi
import com.develop.dubhad.sunflower.main.tag.models.domain.Tag
import io.reactivex.Single
import javax.inject.Inject

class TagRepository @Inject constructor(private val sunflowerApi: SunflowerApi) {

    fun fetchTags(): Single<List<Tag>> {
        return sunflowerApi.getTags()
            .map { it.results.map(::Tag) }
    }

    fun fetchRecipeTags(recipeId: Int): Single<List<Tag>> {
        return sunflowerApi.getRecipeTags(recipeId)
            .map { it.map(::Tag) }
    }

    // TODO Replace with API call
    fun fetchTagsByQuery(query: String): Single<List<Tag>> {
        return fetchTags().map { it.filter { tag -> tag.title.startsWith(query) } }
    }
}
