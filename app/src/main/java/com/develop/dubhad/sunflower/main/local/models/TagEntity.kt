package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.develop.dubhad.sunflower.main.tag.models.domain.Tag

@Entity
data class TagEntity(@PrimaryKey val tagId: Int, val title: String, val popularity: Int) {
    constructor(tag: Tag) : this(
        tagId = tag.id,
        title = tag.title,
        popularity = tag.popularity
    )
}
