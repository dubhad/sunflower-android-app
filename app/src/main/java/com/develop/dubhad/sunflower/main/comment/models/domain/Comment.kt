package com.develop.dubhad.sunflower.main.comment.models.domain

import java.util.Date

data class Comment(
    val id: Int,
    val content: String,
    val photo: String,
    val publishDate: Date,
    val authorId: Int,
    val recipeId: Int
)
