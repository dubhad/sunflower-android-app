package com.develop.dubhad.sunflower.authentication.models.errors

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegisterErrorResponse(
    @Json(name = "username") val usernameErrors: List<String>? = null,
    @Json(name = "email") val emailErrors: List<String>? = null,
    @Json(name = "password1") val passwordErrors: List<String>? = null,
    @Json(name = "password2") val repeatedPasswordErrors: List<String>? = null,
    @Json(name = "non_field_errors") val otherErrors: List<String>? = null
)
