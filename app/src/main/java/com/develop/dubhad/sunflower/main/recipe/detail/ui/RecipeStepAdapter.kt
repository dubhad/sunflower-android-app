package com.develop.dubhad.sunflower.main.recipe.detail.ui

import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.develop.dubhad.sunflower.utils.extensions.inflate
import com.develop.dubhad.sunflower.utils.extensions.load
import kotlinx.android.synthetic.main.image_popup.view.*
import kotlinx.android.synthetic.main.item_recipe_step.view.*

class RecipeStepAdapter(private val activity: FragmentActivity) : ListAdapter<RecipeStep, RecipeStepAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_recipe_step))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val step = getItem(position)
        holder.bind(step, activity)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: RecipeStep, activity: FragmentActivity) {
            with(itemView) {
                recipe_step_item_number.text = item.number.toString()
                recipe_step_item_description.text = item.description
                if (item.photo.isBlank()) {
                    recipe_step_item_photo.visibility = View.GONE
                } else {
                    recipe_step_item_photo.visibility = View.VISIBLE
                    recipe_step_item_photo.load(item.photo)
                    recipe_step_item_photo.setOnClickListener { view ->
                        // TODO rewrite (maybe interface)
                        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val popupView = inflater.inflate(R.layout.image_popup, null)
                        popupView.image_full.load(item.photo)
                        val width = LinearLayout.LayoutParams.MATCH_PARENT
                        val height = LinearLayout.LayoutParams.WRAP_CONTENT
                        val popupWindow = PopupWindow(popupView, width, height, true)
                        popupWindow.showAtLocation(view.rootView, Gravity.CENTER, 0, 0)
                        popupView.setOnTouchListener { _, _ ->
                            popupWindow.dismiss()
                            true
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RecipeStep>() {

            override fun areItemsTheSame(oldItem: RecipeStep, newItem: RecipeStep): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: RecipeStep, newItem: RecipeStep): Boolean {
                return oldItem == newItem
            }
        }
    }
}
