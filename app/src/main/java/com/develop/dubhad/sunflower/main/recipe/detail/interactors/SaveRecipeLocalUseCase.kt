package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Completable
import javax.inject.Inject

class SaveRecipeLocalUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(recipeDetail: RecipeDetail): Completable {
        return recipeRepository.saveRecipeLocal(recipeDetail)
    }
}
