package com.develop.dubhad.sunflower.main.base.di

import com.develop.dubhad.sunflower.main.cookbook.ui.CookbookListFragment
import com.develop.dubhad.sunflower.main.recipe.detail.ui.RecipeDetailFragment
import com.develop.dubhad.sunflower.main.recipe.list.ui.RecipeListFragment
import com.develop.dubhad.sunflower.main.tag.ui.TagListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeCookbookListFragment(): CookbookListFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeListFragment(): RecipeListFragment

    @ContributesAndroidInjector
    abstract fun contributeRecipeDetailFragment(): RecipeDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeTagListFragment(): TagListFragment
}
