package com.develop.dubhad.sunflower.utils.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
