package com.develop.dubhad.sunflower.main.cookbook.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.main.cookbook.interactors.FetchCookbooksUseCase
import com.develop.dubhad.sunflower.main.cookbook.models.domain.Cookbook
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class CookbookListViewModel @Inject constructor(
    private val fetchCookbooksUseCase: FetchCookbooksUseCase
) : BaseViewModel() {

    private val _resource: MutableLiveData<Resource<List<Cookbook>>> = MutableLiveData()

    val resource: LiveData<Resource<List<Cookbook>>>
        get() = _resource

    fun fetchCookbooks() {
        fetchCookbooksUseCase.execute()
            .subscribeWithResource(_resource)
            .addTo(disposables)
    }
}
