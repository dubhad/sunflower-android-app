package com.develop.dubhad.sunflower.utils.di

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.develop.dubhad.sunflower.App
import com.develop.dubhad.sunflower.base.di.components.DaggerAppComponent
import com.develop.dubhad.sunflower.utils.ActivityLifecycleCallbacksHelper
import dagger.android.AndroidInjection
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection

object AppInjector {
    fun init(app: App) {
        DaggerAppComponent.factory()
            .create(app)
            .inject(app)

        app.registerActivityLifecycleCallbacks(ActivityLifecycleCallbacksHelper(onCreated = {
            handleActivity(it)
        }))
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasAndroidInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true
                )
        }
    }
}
