package com.develop.dubhad.sunflower.main.recipe.detail.ui

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.develop.dubhad.sunflower.utils.extensions.inflate
import java.text.NumberFormat
import kotlinx.android.synthetic.main.item_ingredient.view.*

class IngredientAdapter : ListAdapter<Ingredient, IngredientAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_ingredient))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ingredient = getItem(position)
        holder.bind(ingredient)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Ingredient) {
            with(itemView) {
                ingredient_item_product_name.text = item.productName
                // TODO Write better solution later
                dots.text = ".".repeat(1000)
                if (item.quantity == 0f) {
                    ingredient_item_quantity.visibility = View.GONE
                } else {
                    ingredient_item_quantity.visibility = View.VISIBLE
                    ingredient_item_quantity.text = NumberFormat.getInstance().format(item.quantity)
                }
                ingredient_item_weight_measure.text = item.weightMeasure
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Ingredient>() {

            override fun areItemsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean {
                return oldItem == newItem
            }
        }
    }
}
