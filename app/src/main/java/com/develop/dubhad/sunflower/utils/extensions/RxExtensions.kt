package com.develop.dubhad.sunflower.utils.extensions

import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.base.models.Resource
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.Disposable

fun <T> Single<T>.subscribeWithResource(
    resource: MutableLiveData<Resource<T>>,
    onLoading: (Disposable) -> Unit = { resource.postValue(Resource.loading()) },
    onError: (Throwable) -> Unit = { resource.postValue(Resource.error(it.localizedMessage)) },
    onSuccess: (T) -> Unit = { resource.postValue(Resource.success(it)) }
): Disposable {
    return this.doOnSubscribe(onLoading)
        .subscribe(onSuccess, onError)
}

fun Completable.subscribeWithResource(
    resource: MutableLiveData<Resource<Nothing>>,
    onLoading: (Disposable) -> Unit = { resource.postValue(Resource.loading()) },
    onError: (Throwable) -> Unit = { resource.postValue(Resource.error(it.localizedMessage)) },
    onSuccess: () -> Unit = { resource.postValue(Resource.success(null)) }
): Disposable {
    return this.doOnSubscribe(onLoading)
        .subscribe(onSuccess, onError)
}
