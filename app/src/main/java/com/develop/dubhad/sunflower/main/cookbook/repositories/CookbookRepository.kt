package com.develop.dubhad.sunflower.main.cookbook.repositories

import com.develop.dubhad.sunflower.base.services.SunflowerApi
import com.develop.dubhad.sunflower.main.cookbook.models.domain.Cookbook
import com.develop.dubhad.sunflower.utils.TokenManager
import io.reactivex.Single
import javax.inject.Inject

class CookbookRepository @Inject constructor(private val sunflowerApi: SunflowerApi) {

    @Inject
    lateinit var tokenManager: TokenManager

    fun fetchCookbooks(): Single<List<Cookbook>> {
        return sunflowerApi.getUserCookbooks(tokenManager.getToken())
            .map { it.results.map(::Cookbook) }
    }
}
