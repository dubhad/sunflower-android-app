package com.develop.dubhad.sunflower.utils

import android.util.Patterns
import android.widget.EditText

object TextUtil {

    fun isFieldEmpty(field: EditText): Boolean = field.text.isNullOrBlank()

    fun isEmailValid(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()
}
