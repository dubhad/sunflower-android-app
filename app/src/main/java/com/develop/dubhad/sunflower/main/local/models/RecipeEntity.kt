package com.develop.dubhad.sunflower.main.local.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import java.util.Date

@Entity(
    foreignKeys = [ForeignKey(
        entity = AuthorEntity::class,
        parentColumns = ["id"],
        childColumns = ["authorId"]
    )]
)
data class RecipeEntity(
    @PrimaryKey val recipeId: Int,
    val title: String,
    val description: String,
    val photo: String,
    val publishDate: Date,
    val cookingTime: Int,
    val personCount: Int,
    val rating: Float,
    val authorId: Int
) {
    constructor(recipe: Recipe) : this(
        recipeId = recipe.id,
        title = recipe.title,
        description = recipe.description,
        photo = recipe.photo,
        publishDate = recipe.publishDate,
        cookingTime = recipe.cookingTime,
        personCount = recipe.personCount,
        rating = recipe.rating,
        authorId = recipe.author.id
    )
}
