package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.RecipeDetail
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Single
import javax.inject.Inject

class GetLocalRecipeUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(recipeId: Int): Single<RecipeDetail> {
        return recipeRepository.getLocalRecipe(recipeId)
    }
}
