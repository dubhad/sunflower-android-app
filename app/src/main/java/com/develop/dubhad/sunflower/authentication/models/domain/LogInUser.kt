package com.develop.dubhad.sunflower.authentication.models.domain

data class LogInUser(val username: String, val password: String, val email: String? = null)
