package com.develop.dubhad.sunflower.authentication.repositories

import com.develop.dubhad.sunflower.authentication.models.domain.LogInUser
import com.develop.dubhad.sunflower.authentication.models.domain.RegistrationUser
import com.develop.dubhad.sunflower.authentication.models.requests.LoginRequest
import com.develop.dubhad.sunflower.authentication.models.requests.RegisterRequest
import com.develop.dubhad.sunflower.base.services.SunflowerApi
import io.reactivex.Single
import javax.inject.Inject

class AuthenticationRepository @Inject constructor(private val sunflowerApi: SunflowerApi) {

    fun login(user: LogInUser): Single<String> {
        val response = sunflowerApi.login(
            LoginRequest(
                user.username,
                user.password
            )
        )
        return response.map { it.key }
    }

    fun register(user: RegistrationUser): Single<String> {
        return sunflowerApi.register(RegisterRequest(user))
            .map { it.key }
    }
}
