package com.develop.dubhad.sunflower.main.tag.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.local.models.TagEntity
import com.develop.dubhad.sunflower.main.tag.models.responses.TagResponse
import com.develop.dubhad.sunflower.utils.ParsingUtil

data class Tag(val id: Int, val title: String, val popularity: Int) {
    constructor(response: TagResponse) : this(
        id = response.tagId ?: throw ParsingServerException(),
        title = response.title ?: throw ParsingServerException(),
        popularity = response.popularity ?: throw ParsingServerException()
    ) {
        if (ParsingUtil.isAnyNumberZero(id) || ParsingUtil.isAnyStringBlank(title)) {
            throw ParsingServerException()
        }
    }
    constructor(entity: TagEntity) : this(
        id = entity.tagId,
        title = entity.title,
        popularity = entity.popularity
    )
}
