package com.develop.dubhad.sunflower.authentication.di

import com.develop.dubhad.sunflower.authentication.ui.LogInFragment
import com.develop.dubhad.sunflower.authentication.ui.RegistrationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AuthFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeLogInFragment(): LogInFragment

    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragment(): RegistrationFragment
}
