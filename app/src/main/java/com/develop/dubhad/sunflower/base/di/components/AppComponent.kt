package com.develop.dubhad.sunflower.base.di.components

import android.app.Application
import com.develop.dubhad.sunflower.App
import com.develop.dubhad.sunflower.base.di.modules.ActivityModule
import com.develop.dubhad.sunflower.base.di.modules.AppModule
import com.develop.dubhad.sunflower.base.di.modules.DatabaseModule
import com.develop.dubhad.sunflower.base.di.modules.NetworkModule
import com.develop.dubhad.sunflower.base.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        AppModule::class,
        ActivityModule::class
    ]
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    fun inject(app: App)
}
