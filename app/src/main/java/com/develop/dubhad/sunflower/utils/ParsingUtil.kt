package com.develop.dubhad.sunflower.utils

object ParsingUtil {

    fun isAnyStringBlank(vararg strings: String): Boolean = strings.any { it.isBlank() }

    fun isAnyNumberZero(vararg numbers: Int): Boolean = numbers.any { it == 0 }
}
