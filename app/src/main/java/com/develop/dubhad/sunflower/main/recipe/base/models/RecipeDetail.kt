package com.develop.dubhad.sunflower.main.recipe.base.models

import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import com.develop.dubhad.sunflower.main.local.models.RecipeDetailEntity
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Ingredient
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.RecipeStep
import com.develop.dubhad.sunflower.main.tag.models.domain.Tag

data class RecipeDetail(
    val recipe: Recipe,
    val ingredients: List<Ingredient>,
    val steps: List<RecipeStep>,
    val tags: List<Tag>
) {
    constructor(entity: RecipeDetailEntity, authorEntity: AuthorEntity) : this(
        recipe = Recipe(entity.recipe, authorEntity),
        ingredients = entity.ingredients.map(::Ingredient),
        steps = entity.steps.map(::RecipeStep),
        tags = entity.tags.map(::Tag)
    )
}
