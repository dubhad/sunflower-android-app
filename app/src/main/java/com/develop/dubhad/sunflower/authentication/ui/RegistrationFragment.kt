package com.develop.dubhad.sunflower.authentication.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.authentication.models.domain.RegistrationUser
import com.develop.dubhad.sunflower.authentication.viewmodels.RegisterViewModel
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.base.ui.MainActivity
import com.develop.dubhad.sunflower.utils.PrefsManager
import com.develop.dubhad.sunflower.utils.TextUtil.isEmailValid
import com.develop.dubhad.sunflower.utils.TextUtil.isFieldEmpty
import com.develop.dubhad.sunflower.utils.TokenManager
import com.develop.dubhad.sunflower.utils.di.Injectable
import com.develop.dubhad.sunflower.utils.extensions.hideKeyboard
import kotlinx.android.synthetic.main.fragment_registration.*
import javax.inject.Inject

class RegistrationFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var tokenManager: TokenManager

    @Inject
    lateinit var prefsManager: PrefsManager

    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        registerViewModel = ViewModelProvider(this, viewModelFactory)
            .get(RegisterViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        log_in_link_button.setOnClickListener {
            val direction =
                RegistrationFragmentDirections.actionRegistrationFragmentToLogInFragment()
            it.findNavController().navigate(direction)
        }

        register_button.setOnClickListener {
            requireActivity().hideKeyboard()
            register()
        }

        skip_button.setOnClickListener {
            prefsManager.prefSkipAuth = true
            openMainActivity()
        }

        watchFields()

        registerViewModel.tokenResource.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    onRegisterSuccess(it.data)
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(it.errorMessage)
                }
                Status.LOADING -> {
                    showLoadingBar()
                }
            }
        })
    }

    private fun onRegisterSuccess(token: String) {
        tokenManager.saveToken(token)
        openMainActivity()
    }

    private fun openMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun register() {
        if (isFieldsValid()) {
            val user = RegistrationUser(
                login_field.text.toString(),
                email_field.text.toString(),
                password_field.text.toString(),
                repeated_password_field.text.toString()
            )
            registerViewModel.register(user)
        } else {
            checkFieldErrors()
        }
    }

    private fun isFieldsValid(): Boolean {
        return !isEmptyFields() && isEmailFieldValid() && isPasswordFieldsMatch()
    }

    private fun isEmptyFields(): Boolean {
        return isLoginFieldEmpty() ||
            isEmailFieldEmpty() ||
            isPasswordFieldEmpty() ||
            isRepeatedPasswordFieldEmpty()
    }

    private fun checkFieldErrors() {
        checkEmptyFields()
        checkEmailField()
        checkPasswordsMatchFields()
    }

    private fun checkEmptyFields() {
        if (isLoginFieldEmpty()) {
            login_layout.error = getString(R.string.empty_field_error)
        } else {
            login_layout.error = null
        }

        if (isEmailFieldEmpty()) {
            email_layout.error = getString(R.string.empty_field_error)
        } else {
            email_layout.error = null
        }

        if (isPasswordFieldEmpty()) {
            password_layout.error = getString(R.string.empty_field_error)
        } else {
            password_layout.error = null
        }

        if (isRepeatedPasswordFieldEmpty()) {
            repeated_password_layout.error = getString(R.string.empty_field_error)
        } else {
            repeated_password_layout.error = null
        }
    }

    private fun checkEmailField() {
        if (!isEmailFieldEmpty()) {
            if (!isEmailFieldValid()) {
                email_layout.error = getString(R.string.invalid_email_error)
            } else {
                email_layout.error = null
            }
        }
    }

    private fun checkPasswordsMatchFields() {
        if (!isPasswordFieldEmpty()) {
            if (!isPasswordFieldsMatch()) {
                repeated_password_layout.error = getString(R.string.passwords_dont_match_error)
            } else {
                repeated_password_layout.error = null
            }
        }
    }

    private fun isLoginFieldEmpty(): Boolean = isFieldEmpty(login_field)

    private fun isEmailFieldEmpty(): Boolean = isFieldEmpty(email_field)

    private fun isPasswordFieldEmpty(): Boolean = isFieldEmpty(password_field)

    private fun isRepeatedPasswordFieldEmpty(): Boolean = isFieldEmpty(repeated_password_field)

    private fun isEmailFieldValid(): Boolean = isEmailValid(email_field.text.toString())

    private fun isPasswordFieldsMatch(): Boolean {
        return password_field.text.toString() == repeated_password_field.text.toString()
    }

    private fun watchFields() {
        watchLoginField()
        watchEmailField()
        watchPasswordField()
        watchRepeatedPasswordField()
    }

    private fun watchLoginField() {
        login_field.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                login_layout.error = null
            }
        }
    }

    private fun watchEmailField() {
        email_field.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                email_layout.error = null
            }
        }
    }

    private fun watchPasswordField() {
        password_field.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                password_layout.error = null

                if (it.toString() != repeated_password_field.text.toString()) {
                    repeated_password_layout.error = getString(R.string.passwords_dont_match_error)
                } else {
                    repeated_password_layout.error = null
                }
            }
        }
    }

    private fun watchRepeatedPasswordField() {
        repeated_password_field.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                if (it.toString() != password_field.text.toString()) {
                    repeated_password_layout.error = getString(R.string.passwords_dont_match_error)
                } else {
                    repeated_password_layout.error = null
                }
            }
        }
    }
}
