package com.develop.dubhad.sunflower.main.recipe.detail.interactors

import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.main.recipe.base.repositories.RecipeRepository
import io.reactivex.Completable
import javax.inject.Inject

class DeleteLocalRecipeUseCase @Inject constructor(private val recipeRepository: RecipeRepository) {

    fun execute(recipe: Recipe): Completable {
        return recipeRepository.deleteLocalRecipe(recipe)
    }
}
