package com.develop.dubhad.sunflower.main.cookbook.interactors

import com.develop.dubhad.sunflower.main.cookbook.models.domain.Cookbook
import com.develop.dubhad.sunflower.main.cookbook.repositories.CookbookRepository
import io.reactivex.Single
import javax.inject.Inject

class FetchCookbooksUseCase @Inject constructor(
    private val cookbookRepository: CookbookRepository
) {
    fun execute(): Single<List<Cookbook>> = cookbookRepository.fetchCookbooks()
}
