package com.develop.dubhad.sunflower.main.tag.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagResponse(
    @Json(name = "tag_id") val tagId: Int?,
    @Json(name = "title") val title: String?,
    @Json(name = "popularity") val popularity: Int?
)
