package com.develop.dubhad.sunflower.main.tag.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.base.models.Status
import com.develop.dubhad.sunflower.base.ui.BaseFragment
import com.develop.dubhad.sunflower.main.tag.viewmodels.TagListViewModel
import com.develop.dubhad.sunflower.utils.di.Injectable
import kotlinx.android.synthetic.main.fragment_tag_list.*
import javax.inject.Inject

class TagListFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TagListViewModel

    private lateinit var tagAdapter: TagAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.fragment_tag_list, container, false)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(TagListViewModel::class.java)

        viewModel.fetchTags()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val divider = DividerItemDecoration(tag_list.context, DividerItemDecoration.VERTICAL)
        tag_list.addItemDecoration(divider)

        tagAdapter = TagAdapter()
        tag_list.adapter = tagAdapter
        registerAdapterDataObserver(tagAdapter)
        subscribeUi(tagAdapter)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_tag_list, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        searchView.maxWidth = Int.MAX_VALUE
        searchView.queryHint = context?.resources?.getString(R.string.filter_hint)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = false

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.search(newText ?: "")
                return true
            }
        })
    }

    private fun subscribeUi(adapter: TagAdapter) {
        viewModel.resource.observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    hideLoadingBar()
                    adapter.submitList(resource.data)
                }
                Status.ERROR -> {
                    hideLoadingBar()
                    showError(resource.errorMessage)
                }
                Status.LOADING -> showLoadingBar()
            }
        })
    }

    private fun registerAdapterDataObserver(adapter: TagAdapter) {
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                tag_list.scrollToPosition(0)
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                tag_list.scrollToPosition(0)
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                tag_list.scrollToPosition(0)
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
                tag_list.scrollToPosition(0)
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                tag_list.scrollToPosition(0)
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                tag_list.scrollToPosition(0)
            }
        })
    }
}
