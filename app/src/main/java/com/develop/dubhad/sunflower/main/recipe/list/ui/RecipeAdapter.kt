package com.develop.dubhad.sunflower.main.recipe.list.ui

import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.main.recipe.base.models.domain.Recipe
import com.develop.dubhad.sunflower.utils.FormatUtil
import com.develop.dubhad.sunflower.utils.extensions.inflate
import com.develop.dubhad.sunflower.utils.extensions.load
import kotlinx.android.synthetic.main.item_recipe.view.*

class RecipeAdapter(private val isLocal: Boolean) : ListAdapter<Recipe, RecipeAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_recipe))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = getItem(position)
        holder.bind(createOnClickListener(recipe.id, recipe.title, isLocal), recipe)
    }

    private fun createOnClickListener(recipeId: Int, recipeTitle: String, local: Boolean): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                RecipeListFragmentDirections.actionRecipeListFragmentToRecipeDetailFragment(
                    recipeId,
                    recipeTitle,
                    local
                )
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listener: View.OnClickListener, item: Recipe) {
            with(itemView) {
                setOnClickListener(listener)

                if (item.photo.isBlank()) {
                    recipe_item_photo.visibility = View.GONE
                } else {
                    recipe_item_photo.visibility = View.VISIBLE
                    recipe_item_photo.load(item.photo)
                }

                recipe_item_title.text = item.title
                material_rating_bar.rating = item.rating
                recipe_item_rating.text = String.format("%.1f", item.rating)

                if (item.description.isBlank()) {
                    recipe_item_description.visibility = View.GONE
                } else {
                    recipe_item_description.visibility = View.VISIBLE
                    recipe_item_description.text = item.description
                }

                if (item.cookingTime == 0) {
                    cooking_time_group.visibility = View.GONE
                } else {
                    cooking_time_group.visibility = View.VISIBLE
                    recipe_item_cooking_time.text = FormatUtil.getReadableTime(context, item.cookingTime)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Recipe>() {

            override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
                return oldItem == newItem
            }
        }
    }
}
