package com.develop.dubhad.sunflower.authentication.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.authentication.interactors.RegisterUseCase
import com.develop.dubhad.sunflower.authentication.models.domain.RegistrationUser
import com.develop.dubhad.sunflower.authentication.models.errors.RegisterErrorResponse
import com.develop.dubhad.sunflower.base.models.Resource
import com.develop.dubhad.sunflower.base.viewmodels.BaseViewModel
import com.develop.dubhad.sunflower.utils.StringsManager
import com.develop.dubhad.sunflower.utils.extensions.subscribeWithResource
import io.reactivex.rxkotlin.addTo
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val stringsManager: StringsManager,
    private val registerErrorConverter: Converter<ResponseBody, RegisterErrorResponse>,
    private val registerUseCase: RegisterUseCase
) : BaseViewModel() {

    private val _tokenResource: MutableLiveData<Resource<String>> = MutableLiveData()

    val tokenResource: LiveData<Resource<String>>
        get() = _tokenResource

    fun register(user: RegistrationUser) {
        registerUseCase.execute(user)
            .subscribeWithResource(_tokenResource, onError = {
                val message = parseError(it)
                _tokenResource.postValue(Resource.error(message))
            })
            .addTo(disposables)
    }

    private fun parseError(throwable: Throwable): String {
        val error = throwable as? HttpException

        return if (error?.code() == HttpURLConnection.HTTP_BAD_REQUEST) {
            val errorBody = error.response()?.errorBody()
            errorBody?.let {
                val errorResponse = registerErrorConverter.convert(errorBody)
                errorResponse?.let { getParsedError(it) }
            } ?: stringsManager.getString(R.string.unknown_error)
        } else {
            throwable.localizedMessage
        }
    }

    private fun getParsedError(errorResponse: RegisterErrorResponse): String {
        return with(errorResponse) {
            when {
                !usernameErrors.isNullOrEmpty() -> {
                    usernameErrors.first()
                }
                !emailErrors.isNullOrEmpty() -> {
                    emailErrors.first()
                }
                !passwordErrors.isNullOrEmpty() -> {
                    passwordErrors.first()
                }
                !repeatedPasswordErrors.isNullOrEmpty() -> {
                    repeatedPasswordErrors.first()
                }
                !otherErrors.isNullOrEmpty() -> {
                    otherErrors.first()
                }
                else -> {
                    stringsManager.getString(R.string.unknown_error)
                }
            }
        }
    }
}
