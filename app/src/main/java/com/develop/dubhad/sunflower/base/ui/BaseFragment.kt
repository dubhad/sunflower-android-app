package com.develop.dubhad.sunflower.base.ui

import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.develop.dubhad.sunflower.utils.extensions.snack
import kotlinx.android.synthetic.main.view_loading_bar.*

abstract class BaseFragment : Fragment() {

    protected fun showError(message: String) {
        requireView().snack(message)
    }

    protected fun showLoadingBar() {
        showBar(true)
    }

    protected fun hideLoadingBar() {
        showBar(false)
    }

    private fun showBar(show: Boolean) {
        if (loading_bar != null && loading_bar is ProgressBar) {
            loading_bar.visibility = if (show) View.VISIBLE else View.GONE
        } else {
            throw IllegalStateException("Loading bar not found in your layout")
        }
    }
}
