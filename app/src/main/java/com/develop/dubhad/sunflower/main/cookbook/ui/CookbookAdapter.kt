package com.develop.dubhad.sunflower.main.cookbook.ui

import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.develop.dubhad.sunflower.R
import com.develop.dubhad.sunflower.main.cookbook.models.domain.Cookbook
import com.develop.dubhad.sunflower.utils.extensions.inflate
import kotlinx.android.synthetic.main.item_cookbook.view.*

class CookbookAdapter : ListAdapter<Cookbook, CookbookAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_cookbook))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cookbook = getItem(position)
        holder.bind(createOnClickListener(cookbook.id, cookbook.name), cookbook)
    }

    private fun createOnClickListener(cookbookId: Int, cookbookName: String): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                CookbookListFragmentDirections.actionCookbookListFragmentToRecipeListFragment(
                    cookbookId,
                    cookbookName
                )
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listener: View.OnClickListener, item: Cookbook) {
            with(itemView) {
                setOnClickListener(listener)
                cookbook_item_name.text = item.name
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Cookbook>() {

            override fun areItemsTheSame(oldItem: Cookbook, newItem: Cookbook): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Cookbook, newItem: Cookbook): Boolean {
                return oldItem == newItem
            }
        }
    }
}
