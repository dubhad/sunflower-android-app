package com.develop.dubhad.sunflower.main.cookbook.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.cookbook.models.responses.CookbookResponse
import com.develop.dubhad.sunflower.utils.ParsingUtil

data class Cookbook(
    val id: Int,
    val name: String
) {
    constructor(response: CookbookResponse) : this(
        id = response.cookbookId ?: throw ParsingServerException(),
        name = response.name ?: throw ParsingServerException()
    ) {
        if (ParsingUtil.isAnyNumberZero(id) || ParsingUtil.isAnyStringBlank(name)) {
            throw ParsingServerException()
        }
    }
}
