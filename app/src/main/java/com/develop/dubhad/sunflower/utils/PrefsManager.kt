package com.develop.dubhad.sunflower.utils

import android.content.SharedPreferences
import dagger.Reusable
import javax.inject.Inject

@Reusable
class PrefsManager @Inject constructor(private val sharedPrefs: SharedPreferences) {

    companion object {
        private const val PREF_SKIP_AUTH = "PREF_SKIP_AUTH"
        private const val PREF_GESTURE_DELAY = "PREF_GESTURE_DELAY"
        private const val PREF_SCROLL_DELTA = "PREF_SCROLL_DELTA"

        private const val DEFAULT_SCROLL_DELTA = 100
    }

    var prefSkipAuth: Boolean
        get() = sharedPrefs.getBoolean(PREF_SKIP_AUTH, false)
        set(value) {
            sharedPrefs.edit()
                .putBoolean(PREF_SKIP_AUTH, value)
                .apply()
        }

    var prefGestureDelay: Int
        get() = sharedPrefs.getInt(PREF_GESTURE_DELAY, 1)
        set(value) {
            sharedPrefs.edit()
                .putInt(PREF_GESTURE_DELAY, value)
                .apply()
        }

    var prefScrollDelta: Int
        get() = sharedPrefs.getInt(PREF_SCROLL_DELTA, DEFAULT_SCROLL_DELTA)
        set(value) {
            sharedPrefs.edit()
                .putInt(PREF_SCROLL_DELTA, value)
                .apply()
        }
}
