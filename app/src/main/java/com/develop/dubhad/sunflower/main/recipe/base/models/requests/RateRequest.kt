package com.develop.dubhad.sunflower.main.recipe.base.models.requests

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RateRequest(@Json(name = "rating") val rating: Float)
