package com.develop.dubhad.sunflower.main.author.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.author.models.responses.AuthorResponse
import com.develop.dubhad.sunflower.main.local.models.AuthorEntity
import com.develop.dubhad.sunflower.utils.ParsingUtil

data class Author(val id: Int, val username: String) {
    constructor(response: AuthorResponse) : this(
        id = response.userId ?: throw ParsingServerException(),
        username = response.username ?: throw ParsingServerException()
    ) {
        if (ParsingUtil.isAnyNumberZero(id) || ParsingUtil.isAnyStringBlank(username)) {
            throw ParsingServerException()
        }
    }
    constructor(entity: AuthorEntity) : this(
        id = entity.id,
        username = entity.username
    )
}
