package com.develop.dubhad.sunflower.base.models

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
