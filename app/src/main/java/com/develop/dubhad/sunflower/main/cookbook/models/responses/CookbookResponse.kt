package com.develop.dubhad.sunflower.main.cookbook.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CookbookResponse(
    @Json(name = "cookbook_id") val cookbookId: Int?,
    @Json(name = "name") val name: String?
)
