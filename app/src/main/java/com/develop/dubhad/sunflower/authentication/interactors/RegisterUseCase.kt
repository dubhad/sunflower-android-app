package com.develop.dubhad.sunflower.authentication.interactors

import com.develop.dubhad.sunflower.authentication.models.domain.RegistrationUser
import com.develop.dubhad.sunflower.authentication.repositories.AuthenticationRepository
import io.reactivex.Single
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    fun execute(user: RegistrationUser): Single<String> = authenticationRepository.register(user)
}
