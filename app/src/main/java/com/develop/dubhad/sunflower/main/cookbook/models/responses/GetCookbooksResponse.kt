package com.develop.dubhad.sunflower.main.cookbook.models.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetCookbooksResponse(@Json(name = "results") val results: List<CookbookResponse>)
