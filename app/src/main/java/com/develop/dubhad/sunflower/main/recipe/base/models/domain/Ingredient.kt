package com.develop.dubhad.sunflower.main.recipe.base.models.domain

import com.develop.dubhad.sunflower.base.exceptions.ParsingServerException
import com.develop.dubhad.sunflower.main.local.models.IngredientEntity
import com.develop.dubhad.sunflower.main.recipe.base.models.responses.IngredientResponse
import com.develop.dubhad.sunflower.utils.ParsingUtil

data class Ingredient(
    val id: Int,
    val quantity: Float,
    val weightMeasure: String,
    val productName: String
) {
    constructor(response: IngredientResponse) : this(
        id = response.ingredientId ?: throw ParsingServerException(),
        quantity = response.quantity ?: throw ParsingServerException(),
        weightMeasure = response.weightMeasure ?: throw ParsingServerException(),
        productName = response.product ?: throw ParsingServerException()
    ) {
        if (ParsingUtil.isAnyNumberZero(id) ||
            ParsingUtil.isAnyStringBlank(weightMeasure, productName)
        ) {
            throw ParsingServerException()
        }
    }
    constructor(entity: IngredientEntity) : this(
        id = entity.id,
        quantity = entity.quantity,
        weightMeasure = entity.weightMeasure,
        productName = entity.productName
    )
}
