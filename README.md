#  ![icon](repo_res/sunflower.png)Sunflower (alpha)
Mobile client of culinary app [Sunflower](https://gitlab.com/sunflower-cookbook/sunflower) for Android.

Despite having main features app is currently released as an *alpha* and is under heavy development. So app may be **unstable** for now.

## Screenshots
![](repo_res/screenshots/sunflower_01.png)
![](repo_res/screenshots/sunflower_02.png)
![](repo_res/screenshots/sunflower_03.png)
![](repo_res/screenshots/sunflower_04.png)
![](repo_res/screenshots/sunflower_05.png)
![](repo_res/screenshots/sunflower_06.png)

## Features
* Log in to your Sunflower account
* Browse your cookbooks
* Browse all Sunflower recipes
* View all recipe details
* Rate recipe
* Browse Sunflower categories
* Filter Sunflower tags
* Browse recipes by tags
* Local data caching

## Upcoming features
* Save choosed recipes locally
* Manage cookbooks
* Manage recipes
* Manage tags (categories)
* Add recipes to cookbooks
* Browse comments

## Requirements
Android 5.0 Lollipop (API 21) or above.

## Download
You can download latest published version [here](http://46.101.25.169:8123/data/com.develop.dubhad.sunflower.debug.apk).

## Building
This project uses the Gradle build system. To build this project, use the
`gradlew build` command or use "Import Project" in Android Studio.

## About project
App written in Kotlin and implementing MVVM architecture pattern.

### Libraries used
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture) - A collection of libraries that help you design robust, testable, and maintainable apps.
  + [Room](https://developer.android.com/topic/libraries/architecture/room) - Provides an abstraction layer over SQLite to allow for more robust database access while harnessing the full power of SQLite.
  + [Navigation](https://developer.android.com/guide/navigation/) - Handle everything needed for in-app navigation.
  + [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle) - Create a UI that automatically responds to lifecycle events.
  + [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Build data objects that notify views when the underlying database changes.
  + [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Store and manage UI-related data in a lifecycle conscious way.
  + [Data Binding](https://developer.android.com/topic/libraries/data-binding/) - Allows to bind UI components in your layouts to data sources in your app using a declarative format rather than programmatically.
* [RxJava](https://github.com/ReactiveX/RxJava) - A library for composing asynchronous and event-based programs by using observable sequences.
* [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client.
* [Moshi](https://github.com/square/moshi) - A modern JSON library.
* [Glide](https://github.com/bumptech/glide) - A fast and efficient media management and image loading framework.
